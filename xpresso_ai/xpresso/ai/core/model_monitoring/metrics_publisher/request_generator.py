"""
    Class implementation of request metrics generator
"""

__all__ = ["RequestGenerator"]
__author__ = ["Shlok Chaudhari"]


from datetime import datetime
from xpresso.ai.core.model_monitoring.metrics_publisher.metric_type import Request
from xpresso.ai.core.model_monitoring.metrics_publisher.abstract_metric_generator import \
    AbstractMetricGenerator
from xpresso.ai.core.commons.exceptions.xpr_exceptions import InvalidModelMonitoringData
from xpresso.ai.core.commons.utils.constants import METRIC_TYPE_REQUEST, MODEL_ID, MODEL_REQUEST_ID, \
    TYPE_KEY, TIME


class RequestGenerator(AbstractMetricGenerator):
    """
        This class formulates request metrics for model monitoring
    """

    def __init__(self, model_id: str, request_id: str, request_data: dict):
        super().__init__(model_id, METRIC_TYPE_REQUEST)
        self._request_id = request_id
        self._request_data = request_data
        self._request_schema = Request.get()
        self._generate_metrics()

    def _generate_metrics(self):
        """
            Generates request metrics to be sent for monitoring
        Returns:
            metrics(dict)
        """
        self._request_data.update({
            MODEL_ID: self._model_id,
            MODEL_REQUEST_ID: self._request_id,
            TYPE_KEY: self._metric_type,
            TIME: datetime.utcnow().isoformat()
        })
        if not set(list(self._request_data.keys())) == set(self._request_schema):
            raise InvalidModelMonitoringData("Request table schema is not being followed")

    def get_metrics(self):
        """
            Get request metrics in publishable schema
        Returns:
            metrics(dict)
        """
        return self._request_data
