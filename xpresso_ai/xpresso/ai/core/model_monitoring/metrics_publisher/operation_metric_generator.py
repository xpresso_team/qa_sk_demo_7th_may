"""
    Class implementation of system operation metrics generator
"""

__all__ = ["OperationMetricsGenerator"]
__author__ = ["Shlok Chaudhari"]


from datetime import datetime
from xpresso.ai.core.model_monitoring.metrics_publisher.metric_type import Operation
from xpresso.ai.core.model_monitoring.metrics_publisher.os_resource_usage import OSResourceUsage
from xpresso.ai.core.model_monitoring.metrics_publisher.abstract_metric_generator import \
    AbstractMetricGenerator

from xpresso.ai.core.commons.utils.constants import METRIC_TYPE_OPERATION


class OperationMetricsGenerator(AbstractMetricGenerator):
    """
        This class generates all the system
        operational metrics for model monitoring
    """

    def __init__(self, model_id: str):
        super().__init__(model_id, METRIC_TYPE_OPERATION)

    def _generate_metrics(self) -> list:
        """
            Generate all system operation metrics
        Returns:
            metrics(list)
        """
        metrics = []
        os_resource_usage = OSResourceUsage()
        metrics.extend(os_resource_usage.evaluate_cpu_metrics())
        metrics.extend(os_resource_usage.evaluate_cpu_mem_metrics())
        metrics.extend(os_resource_usage.evaluate_gpu_metrics())
        metrics.extend(os_resource_usage.evaluate_gpu_mem_metrics())
        metrics.extend(os_resource_usage.evaluate_root_disk_metrics())
        metrics.extend(os_resource_usage.evaluate_partition_disk_metrics())
        metrics.insert(0, datetime.utcnow().isoformat())
        metrics.insert(0, self._metric_type)
        metrics.insert(0, self._model_id)
        return metrics

    def get_metrics(self) -> dict:
        """
            Get system operation metrics
        Returns:
            metrics(dict)
        """
        self._logger.info("Calculating resource usage metrics")
        resource_usage_metrics = self._generate_metrics()
        metrics_schema = Operation.get()
        self._logger.info("Sent system operational usage metrics")
        return dict(zip(metrics_schema, resource_usage_metrics))
