"""
    Utility module for run metrics
"""

__author__ = ["Shlok Chaudhari"]
__all__ = ["MetricTypes",
           "check_and_drop_invalid_kpis",
           "check_and_drop_invalid_timeseries"]


import enum
from xpresso.ai.core.logging.xpr_log import XprLogger


logger = XprLogger()


class MetricTypes(enum.Enum):
    """
        Supported metric types
    """
    KPIs = "kpis"
    TIME_SERIES = "time_series"
    EXPLAIN_MODEL = "explain_model"

    @staticmethod
    def get_metric_types() -> list:
        return list(map(lambda mt: mt.value, MetricTypes))


def check_and_drop_invalid_kpis(metrics: dict) -> dict:
    """
        Check KPI metrics input
    Args:
        metrics(dict): metrics as key-value pairs
    Returns:
        metrics(dict): empty dict or dict without
         invalid input
    """
    new_metrics = dict()
    new_metrics.update(metrics)
    # Check dictionary
    if not isinstance(metrics, dict):
        msg = "Skip reporting. KPIs not in dictionary"
        print(msg)
        logger.exception(msg)
        return {}
    # Check key-value pairs
    for key, value in metrics.items():
        # Check keys
        if not isinstance(key, str):
            new_metrics.pop(key)
            msg = f"KPI {key} not specified as str\n" \
                  f"Removing {key}: '{value}' from input"
            print(msg)
            logger.exception(msg)
            continue
        # Check values:
        #   1. If value is a list
        if isinstance(value, list):
            if not all(isinstance(ele, float) or
                       isinstance(ele, int) for ele in value):
                new_metrics.pop(key)
                msg = f"All elements in list not float/int\n" \
                      f"Removing '{key}': {value} from input"
                print(msg)
                logger.exception(msg)
            continue
        #   2. If value is a float/int: XOR implementation
        if isinstance(value, float) == isinstance(value, int):
            new_metrics.pop(key)
            msg = f"For key {key}, value cannot be {type(value)}" \
                  f"\nRemoving '{key}': '{value}' from input"
            print(msg)
            logger.exception(msg)
            continue
    return new_metrics


def check_and_drop_invalid_timeseries(metrics: dict) -> dict:
    """
        Check Time-series metrics input
    Args:
        metrics(dict): metrics as key-value pairs
    Returns:
        metrics(dict): empty dict or dict without
         invalid input
    """
    new_metrics = dict()
    new_metrics.update(metrics)
    # Check dictionary
    if not isinstance(metrics, dict):
        msg = "Skip reporting. KPIs not in dictionary"
        print(msg)
        logger.exception(msg)
        return {}
    # Check key-value pairs
    for key, value in metrics.items():
        # Check keys
        if not isinstance(key, str):
            new_metrics.pop(key)
            msg = f"KPI {key} not specified as str\n" \
                  f"Removing {key}: '{value}' from input"
            print(msg)
            logger.exception(msg)
            continue
        # Check values:
        # If value is a float/int: XOR implementation
        if isinstance(value, float) == isinstance(value, int):
            new_metrics.pop(key)
            msg = f"For key {key}, value cannot be {type(value)}" \
                  f"\nRemoving '{key}': '{value}' from input"
            print(msg)
            logger.exception(msg)
            continue
    return new_metrics
