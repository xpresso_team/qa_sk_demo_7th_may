"""
This is the implementation of xgboost component
"""
import sys

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.ml_utils.shap import ShapUtils
from xpresso.ai.core.ml_utils.xgboost import XgboostMetrics


class AbstractXgboostComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn/xgboost API for
    model training and evaluation. It is extended from
    AbstractPipelineComponent, which allows xpresso platform to track and
    manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is
       initiated.
          This method has a single parameter - the experiment run ID. This is
          automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.
    """

    def __init__(self, name, run_name, params_filename,
                 params_commit_id):
        super().__init__(name=name, run_name=run_name,
                         params_filename=params_filename,
                         params_commit_id=params_commit_id)
        self.model = None
        self.x_train = None
        self.y_train = None

    def completed(self, push_exp=True, success=True):
        """"
        The completed method for xgboost component calls the
        report_xgboost_metrics
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases
        """
        try:
            if self.x_train is None or self.y_train is None or not self.model:
                print("Model, x_train or y_train data not "
                      "provided for metric calculation")
            else:
                self.report_xgboost_metrics(self.x_train, self.y_train)
                self.generate_shap_explainability(self.x_train)

                super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def report_xgboost_metrics(self, x_values, y_values):
        """
         The method automatically detects user's model type (
         xgboost.XGBRegressor, xgboost.XGBClassifier,
          xgboost.Booster) and collects, calculates and reports all metrics
          available for Xgboost model
        Args:
            x_values (array_like or sparse matrix): input matrix (either
                training set, validation set, or test set) with shape of (
                n_samples,  n_features)
            y_values : array-like of shape (n_samples,) or (n_samples,
                n_classes) target variable vector or matrix (either training
                set, validation set, or test set)
        """
        xgboost_metrics = XgboostMetrics(self.model, self.run_parameters)
        calculated_metrics = xgboost_metrics.get_all_metrics(x_values, y_values)
        print("Reporting metrics to xpresso.ai")
        # send metrics to xpresso UI
        self.report_kpi_metrics(calculated_metrics)
        self.logger.info("Metrics Reported")

    def generate_shap_explainability(self, x_values):
        """
        Create Shapley plots for the trained model.

        Allows an approximation of shapley values for a given
        model based on a number of samples from data source X
        Args:
            x_values (dataframe, numpy array, or pixel_values) :
                dataset to be explained
        """
        explainer_list = ["TreeExplainer", "KernelExplainer"]
        shap_utils = ShapUtils(self.model, explainer_list, self.run_parameters)
        shap_utils.explain_shap(x_values)
